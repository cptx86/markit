// 	
// 	testfiles/sample.go  3.125.280  2018-10-23T21:23:22.550260-05:00 (CDT)  https://github.com/BradleyA/markit  bradley  zero.cptx86.com 3.125  
// 	   added example on how to run go program 
// 	sample.go  3.38.179  2018-08-04_13:18:57_CDT  https://github.com/BradleyA/markit  uadmin  three-rpi3b.cptx86.com 3.37  
// 	   New release not backward compatible with markchaeck because of formatting 
// 	
//	$ git add foo.sh
package main

import "fmt"

func main() {
    fmt.Println("hello world")
}

